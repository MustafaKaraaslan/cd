package at.kara.cd;

import java.util.ArrayList;
import java.util.List;

public class CD implements Media {
	private String name;
	private List<Song> sList;
	
	
	public CD(String name) {
		super();
		this.name = name;
		this.sList = new ArrayList<Song>();
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	public void addSong(Song songTitle) {
		sList.add(songTitle);
	}


	@Override
	public void loadMedia(Player playerName) {
		// TODO Auto-generated method stub
		for (Song song : sList) {
			playerName.addMedia(song);
		}
	}
	
}
