package at.kara.cd;

import java.util.ArrayList;
import java.util.List;

public class DVD implements Media {
	private String name;
	private List<Title> titleList;
	
	public void DvD(String name) {
		this.name = name;
		this.titleList = new ArrayList<Title>();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public void addTitle(Title titleName) {
		titleList.add(titleName);
	}

	@Override
	public void loadMedia(Player playerName) {
		// TODO Auto-generated method stub
		for (Title title : titleList) {
			playerName.addMedia(title);
		}
	}
}
