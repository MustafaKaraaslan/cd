package at.kara.cd;

public class Title implements Playable {

	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Titel: " + this.getName());
	}

private String name;
	
	public Title(String name) {
		super();
		this.name = name;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
