package at.kara.cd;

import java.util.ArrayList;
import java.util.List;

public class Player {
	private List<Playable>mediaList;

	public Player() {
		super();
		this.mediaList = new ArrayList<Playable>();
		
	}
	
	public void playTotal() {
		for(Playable playable : mediaList) {
			playable.play();
		}
	}
	
	public void addMedia(Playable mediaTitle) {
		mediaList.add(mediaTitle);
		}
	
}
